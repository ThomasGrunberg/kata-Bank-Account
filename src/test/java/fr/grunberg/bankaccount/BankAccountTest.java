package fr.grunberg.bankaccount;

import org.junit.jupiter.api.Test;

import fr.grunberg.bankaccount.bank.AccountTransaction;
import fr.grunberg.bankaccount.bank.Bank;
import fr.grunberg.bankaccount.individuals.Client;
import fr.grunberg.bankaccount.individuals.Individual;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;

/**
 * Tests JUnit 
 * @author Thomas Grunberg
 *
 */
public class BankAccountTest {
	private Bank bankOfArstotzka;
	private Client clientJorji;
	private Individual individualCorman;
	
	@BeforeEach
	public void prepare() {
		bankOfArstotzka = new Bank("Bank of Arstotzka");
		clientJorji = new Client("Jorji", "Costava");
		bankOfArstotzka.createAccount(clientJorji);
		
		individualCorman = new Individual("Corman", "Drex");
	}
	
	@Test
	public void deposit() {
		assertTrue(
			bankOfArstotzka.deposit(clientJorji, LocalDate.of(2021, 10, 27), new BigDecimal(100), individualCorman)
			);
		assertTrue(
			bankOfArstotzka.deposit(clientJorji, LocalDate.of(2021, 10, 29), new BigDecimal(20), individualCorman)
			);
	}

	@Test
	public void withdraw() {
		assertTrue(
			bankOfArstotzka.deposit(clientJorji, LocalDate.of(2021, 10, 27), new BigDecimal(100), individualCorman)
			);
		assertTrue(
			bankOfArstotzka.withdraw(clientJorji, LocalDate.of(2021, 10, 29), new BigDecimal(50), individualCorman)
			);
		assertFalse(
			bankOfArstotzka.withdraw(clientJorji, LocalDate.of(2021, 10, 30), new BigDecimal(220), individualCorman)
			);
	}

	@Test
	public void checkHistory() {
		assertTrue(
			bankOfArstotzka.deposit(clientJorji, LocalDate.of(2021, 10, 27), new BigDecimal(100), individualCorman)
			);
		assertTrue(
			bankOfArstotzka.withdraw(clientJorji, LocalDate.of(2021, 10, 29), new BigDecimal(50), individualCorman)
			);
		assertTrue(
			bankOfArstotzka.deposit(clientJorji, LocalDate.of(2021, 10, 24), new BigDecimal(106), individualCorman)
			);
		assertEquals(new BigDecimal(156), bankOfArstotzka.getBalance(clientJorji));
		List<AccountTransaction> accountTransactions = bankOfArstotzka.getTransactionHistory(clientJorji);
		assertNotNull(accountTransactions);
		assertEquals(3, accountTransactions.size());
		assertEquals(new BigDecimal(106), accountTransactions.get(0).getAmount());
		assertEquals(new BigDecimal(100), accountTransactions.get(1).getAmount());
		assertEquals(new BigDecimal(-50), accountTransactions.get(2).getAmount());
	}
	
	@Test
	public void checkHistoryDecimal() {
		assertTrue(
			bankOfArstotzka.deposit(clientJorji, LocalDate.of(2021, 10, 27), new BigDecimal(100.16), individualCorman)
			);
		assertTrue(
			bankOfArstotzka.withdraw(clientJorji, LocalDate.of(2021, 10, 29), new BigDecimal(48.3), individualCorman)
			);
		assertTrue(
			bankOfArstotzka.deposit(clientJorji, LocalDate.of(2021, 10, 24), new BigDecimal(4), individualCorman)
			);
		assertEquals(new BigDecimal(55.86), bankOfArstotzka.getBalance(clientJorji));
		List<AccountTransaction> accountTransactions = bankOfArstotzka.getTransactionHistory(clientJorji);
		assertNotNull(accountTransactions);
		assertEquals(3, accountTransactions.size());
		assertEquals(new BigDecimal(4), accountTransactions.get(0).getAmount());
		assertEquals(new BigDecimal(100.16), accountTransactions.get(1).getAmount());
		assertEquals(new BigDecimal(-48.3), accountTransactions.get(2).getAmount());
	}
}
