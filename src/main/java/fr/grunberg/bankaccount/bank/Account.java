package fr.grunberg.bankaccount.bank;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fr.grunberg.bankaccount.individuals.Client;
import fr.grunberg.bankaccount.individuals.Individual;

/**
 * Client bank account
 * @author Thomas Grunberg
 *
 */
public class Account {
	private Bank bank;
	private Client client;
	private int number;
	private List<AccountTransaction> transactions = new ArrayList<AccountTransaction>();

	public Account(Bank bank, int number, Client client) {
		this.bank = bank;
		this.number = number;
		this.client = client;
	}
	
	public AccountTransaction addTransaction(Individual debitor, Individual creditor, LocalDate date, BigDecimal amount) throws InvalidTransactionException {
		verifyValidity(debitor, creditor, date, amount);
		AccountTransaction newTransaction = new AccountTransaction(debitor, creditor, date, amount);
		transactions.add(newTransaction);
		return newTransaction;
	}

	/**
	 * Verifies that a transaction is valid
	 * @param debitor
	 * @param creditor
	 * @param date
	 * @param amount
	 * @throws InvalidTransactionException
	 */
	private void verifyValidity(Individual debitor, Individual creditor, LocalDate date, BigDecimal amount) throws InvalidTransactionException {
		if(debitor == null)
			throw new InvalidTransactionException("Unspecified debitor");
		if(creditor == null)
			throw new InvalidTransactionException("Unspecified creditor");
		if(!creditor.equals(client) && !debitor.equals(client))
			throw new InvalidTransactionException("Wrong account");
		if(creditor.equals(client) && amount.compareTo(new BigDecimal(0)) < 0)
			throw new InvalidTransactionException("Incorrect amount for a credit transaction");
		if(debitor.equals(client) && amount.compareTo(new BigDecimal(0)) > 0)
			throw new InvalidTransactionException("Incorrect amount for a debit transaction");
		if(amount.compareTo(new BigDecimal(0)) < 0
				&& amount.abs().compareTo(getBalance()) > 0
				)
			throw new InvalidTransactionException("Account not sufficiently provisionned : " +  getBalance() + " < " + amount);
		if(amount.compareTo(new BigDecimal(0)) == 0)
			throw new InvalidTransactionException("I have better things to do with my time");

	}
	
	public Bank getBank() {
		return bank;
	}

	public int getNumber() {
		return number;
	}

	public Client getClient() {
		return client;
	}
	
	/**
	 * Returns the account balance
	 * @return
	 */
	protected BigDecimal getBalance() {
		BigDecimal balance = new BigDecimal(0);
		for(AccountTransaction t : transactions)
			balance = balance.add(t.getAmount());
		return balance;
	}

	/**
	 * Gets the date-sorted history
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<AccountTransaction> getHistory() {
		if(transactions instanceof ArrayList) {
			List<AccountTransaction> sortedHistory = (List<AccountTransaction>) ((ArrayList<AccountTransaction>) transactions).clone();
			Collections.sort(sortedHistory, new Comparator<AccountTransaction>() {
			    @Override
			    public int compare(AccountTransaction t1, AccountTransaction t2) {
			    	return t1.getDate().compareTo(t2.getDate());
		    }});
			return sortedHistory;
		}
		return transactions;
	}

	@Override
	public int hashCode() {
		return bank.hashCode()
				^ client.hashCode()
				;
	}
}
