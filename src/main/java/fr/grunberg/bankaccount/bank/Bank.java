package fr.grunberg.bankaccount.bank;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.grunberg.bankaccount.individuals.Client;
import fr.grunberg.bankaccount.individuals.Individual;

/**
 * Banking establishment
 * @author Thomas Grunberg
 *
 */
public class Bank {
	private final String name;
	private List<Account> accounts = new ArrayList<Account>();

	public Bank(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void createAccount(Client client) {
		accounts.add(new Account(this, generateUniqueAccountNumber(), client));
	}

	private int generateUniqueAccountNumber() {
		return accounts.size()+1;
	}
	
	/**
	 * Deposit money on the client's account
	 * @param client
	 * @param amount
	 * @param debitor
	 * @return
	 */
	public boolean deposit(Client client, LocalDate date, BigDecimal amount, Individual debitor) {
		Optional<Account> accountToCredit = getAccount(client);
		if(!accountToCredit.isPresent())
			return false;
		try {
			if(accountToCredit.get().addTransaction(debitor, client, date, amount) != null)
				return true;
		}
		catch(InvalidTransactionException e) {
			//System.err.println(e.getMessage());
			return false;
		}
		return false;
	}
	
	/**
	 * Withdraw money from the client's account
	 * @param client
	 * @param amount
	 * @param creditor
	 * @return
	 */
	public boolean withdraw(Client client, LocalDate date, BigDecimal amount, Individual creditor) {
		Optional<Account> accountToCredit = getAccount(client);
		if(!accountToCredit.isPresent())
			return false;
		try {
			accountToCredit.get().addTransaction(client, creditor, date, amount.negate());
			return true;
		}
		catch(InvalidTransactionException e) {
			//System.err.println(e.getMessage());
			return false;
		}
	}
	
	protected Optional<Account> getAccount(Client client) {
		if(client == null)
			return Optional.empty();
		return accounts.stream()
			.filter(account -> client.equals(account.getClient()) )
			.findAny()
			;
	}
	
	public BigDecimal getBalance(Client client) {
		Optional<Account> account = getAccount(client);
		if(!account.isPresent())
			return new BigDecimal(0);
		return account.get().getBalance();
	}

	public List<AccountTransaction> getTransactionHistory(Client client) {
		Optional<Account> account = getAccount(client);
		if(!account.isPresent())
			return null;
		return account.get().getHistory();
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
