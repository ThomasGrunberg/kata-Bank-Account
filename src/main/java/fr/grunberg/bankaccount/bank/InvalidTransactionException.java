package fr.grunberg.bankaccount.bank;

/**
 * An exception for invalid transactions
 * @author Thomas Grunberg
 *
 */
@SuppressWarnings("serial")
public class InvalidTransactionException extends Exception {

	public InvalidTransactionException(String message) {
		super(message);
	}
	
}
