package fr.grunberg.bankaccount.bank;

import java.math.BigDecimal;
import java.time.LocalDate;

import fr.grunberg.bankaccount.individuals.Individual;

/**
 * A transaction on an account
 * @author Thomas Grunberg
 *
 */
public final class AccountTransaction {
	private final Individual debitor, creditor;
	private final LocalDate date;
	private final BigDecimal amount;
	
	public AccountTransaction(Individual debitor, Individual creditor, LocalDate date, BigDecimal amount) {
		this.debitor = debitor;
		this.creditor = creditor;
		this.date = date;
		this.amount = amount;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public Individual getDebitor() {
		return debitor;
	}

	public Individual getCreditor() {
		return creditor;
	}

	public LocalDate getDate() {
		return date;
	}
	
	public String toString() {
		return amount + " (" + date + ")";
	}
	
	@Override
	public int hashCode() {
		return date.hashCode()
				^ debitor.hashCode()
				^ creditor.hashCode()
				^ amount.hashCode()
				;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof AccountTransaction))
			return false;
		AccountTransaction a = (AccountTransaction) o;
		if(!date.equals(a.getDate())
				|| !debitor.equals(a.getDebitor())
				|| !creditor.equals(a.getCreditor())
				|| !amount.equals(a.getAmount())
				)
			return false;
		return true;
	}
}
