package fr.grunberg.bankaccount.individuals;

public class Individual {
	private String firstName, lastName;

	public Individual(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	@Override
	public int hashCode() {
		return firstName.hashCode()
				^ lastName.hashCode()
				;
	}

}
