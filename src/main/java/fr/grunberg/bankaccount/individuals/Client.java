package fr.grunberg.bankaccount.individuals;

/**
 * Bank client
 * @author Thomas Grunberg
 *
 */
public class Client extends Individual {
	public Client(String firstName, String lastName) {
		super(firstName, lastName);
	}
}
